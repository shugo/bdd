/*
 * File : SolutionList.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <cassert>
#include <sstream>

#include "SolutionList.hpp"

namespace BDD
{

    SolutionList::SolutionList()
        : _Solutions(0),
          _Length(0),
          _MaxLength(2),
          _Refcount(0)
    {
        _Solutions =
            reinterpret_cast<Solution*>(malloc(sizeof(Solution) * _MaxLength));
        memset(_Solutions, 0, sizeof(Solution) * _MaxLength);
        _Refcount = new u32();
        *_Refcount = 1;
    }

    SolutionList::SolutionList(const SolutionList& Sol)
        : _Solutions(Sol._Solutions),
          _Length(Sol._Length),
          _MaxLength(Sol._MaxLength),
          _Refcount(Sol._Refcount)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
    }

    SolutionList::~SolutionList()
    {
        *_Refcount = *_Refcount - 1;
        if (*_Refcount == 0)
        {
            if (_Solutions)
            {
                free(_Solutions);
            }
            delete _Refcount;
        }
    }

    SolutionList& SolutionList::operator =(const SolutionList& Sol)
    {
        *_Refcount = *_Refcount - 1;
        if (*_Refcount == 0)
        {
            if (_Solutions)
            {
                free(_Solutions);
            }
            delete _Refcount;
        }
        _Solutions = Sol._Solutions;
        _MaxLength = Sol._MaxLength;
        _Length = Sol._Length;
        _Refcount = Sol._Refcount;
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
        return *this;
    }

    void* SolutionList::operator new(size_t size)
    {
        void* arr = malloc(size * sizeof(SolutionList));
        memset(arr, 0, size * sizeof(SolutionList));
        return arr;
    }

    void SolutionList::operator delete(void* ptr)
    {
        if (ptr)
        {
            free(ptr);
        }
    }

    void* SolutionList::operator new[](size_t size)
    {
        void* arr = malloc(size * sizeof(SolutionList));
        memset(arr, 0, size * sizeof(SolutionList));
        return arr;
    }

    void SolutionList::operator delete[](void* p)
    {
        if (p)
        {
            free(p);
        }
    }

    u32 SolutionList::Length() const
    {
        return _Length;
    }

    void SolutionList::Add(Solution Sol)
    {
        if (_Length >= _MaxLength)
        {
            _MaxLength *= 2;
            _Solutions =
                reinterpret_cast<Solution*>(
                        realloc(_Solutions, _MaxLength * sizeof(Solution)));
            memset(_Solutions + _Length, 0, (_MaxLength - _Length) * sizeof(Solution));
        }
        _Solutions[_Length] = Sol;
        _Length++;
    }

    Solution SolutionList::operator[] (u32 index) const
    {
        assert(index < _Length);
        return _Solutions[index];
    }

    const std::string SolutionList::ToString() const
    {
        std::stringstream ss;
        for (u32 n = 0; n < _Length; n++)
            ss << _Solutions[n].ToString() << std::endl;
        return ss.str();
    }

}
