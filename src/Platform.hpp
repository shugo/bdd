/*
 * File : Platform.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_PLATFORM_HPP_
# define LIBBDD_PLATFORM_HPP_

/* types */

typedef unsigned char      u8;
typedef char               s8;

typedef unsigned short     u16;
typedef short              s16;

typedef unsigned int       u32;
typedef int                s32;

typedef unsigned long long u64;
typedef long long          s64;

typedef void*              ptr;

#define POINTER_TO_U32(p) static_cast<u32>(reinterpret_cast<size_t>(p))
#define POINTER_TO_U64(p) static_cast<u64>(reinterpret_cast<size_t>(p))
#define POINTER_TO_SIZE_T(p) reinterpret_cast<size_t>(p)

/* Platform specific includes */
#include <cstring>
#ifdef WIN32
# include <windows.h>
#else
# include <stdlib.h>
#endif

/* BDD_API_PUBLIC : Export a function or a class */
#ifdef _MSC_VER
# ifdef _DLL
#  define BDD_API_PUBLIC __declspec( dllexport )
# else
#  define BDD_API_PUBLIC __declspec( dllimport )
# endif
# else
#  define BDD_API_PUBLIC
#endif

/* DEBUG_FLAG : define NDEBUG when Visual Studio is in
 * debug mode
 */
#ifndef NDEBUG
# ifdef _MSC_VER
#  ifndef _DEBUG
#   define NDEBUG
#  endif
# endif
#endif

#ifdef _MSC_VER
# define PACK_STRUCT __declspec(align(1))
#else
# define PACK_STRUCT __attribute__((__packed__))
#endif

/* Detect CXX 11 */
#ifdef __GXX_EXPERIMENTAL_CXX0X__
# define WITH_CPP11
#endif
#ifndef WITH_CPP11
# if __cplusplus > 199711L
#  define WITH_CPP11
# endif
#endif

#ifdef WITHOUT_CPP11
# undef WITH_CPP11
#endif

/* CXX 11 Pair Hash */
#ifdef WITH_CPP11
# include <functional>
# include <utility>
namespace std
{
    template <>
    template <class A, class B>
    class hash<std::pair<A, B> >
    {
        public:
            size_t operator()(std::pair<A, B> x) const throw()
            {
                // Use the same hash function has the one from boost
                // http://www.boost.org/doc/libs/1_53_0/doc/html/hash/reference.html#boost.hash_combine
                size_t fhash = hashA(x.first);
                return hashB(x.second) + 0x9e3779b9 + (fhash << 6) + (fhash >> 2);
            }
        private:
            std::hash<A> hashA;
            std::hash<B> hashB;
    };
}
#endif

#endif /* LIBBDD_PLATFORM_HPP_ */
