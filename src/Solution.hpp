/*
 * File : Platform.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_SOLUTION_HPP_
# define LIBBDD_SOLUTION_HPP_

#include <string>

#include "Platform.hpp"
namespace BDD
{
    namespace Internal
    {
        class Solution_p;
    }

    /**
     * @class Solution
     * @brief Stores one solution associated to a function.
     * @note This class is refcounted and encapsulate a reference on an object.
     * By using this class you always target the same object even when you copy
     * an instance of this class.
     */
    class BDD_API_PUBLIC Solution
    {
        public:
            /**
             * @brief Should not be used directly.
             */
            Solution();
            /**
             * @brief Should not be used directly.
             */
            Solution(Internal::Solution_p* Pointer);
            Solution(const Solution& Sol);
            ~Solution();
            Solution& operator =(const Solution& Sol);
            void* operator new(size_t size);
            void operator delete(void* ptr);
            void* operator new[](size_t size);
            void operator delete[](void* ptr);
            /**
             * @brief Test if this solution is a valid one.
             */
            bool IsValid() const;
            /**
             * @brief Get the state of one variable involved
             * in this solution.
             * @brief Name the name of the variable.
             * @note If the specified variable is not involved in this
             * solution the true is returned.
             */
            bool GetState(const char* Name) const;
            /**
             * @brief Get the string representation of this solution.
             */
            const std::string ToString() const;

        private:
            Internal::Solution_p* _Solution;
            int* _Refcount;
    };
}
#endif // LIBBDD_BDD_HPP_
