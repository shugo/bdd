/*
 * File : Function.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <string>

#include "Context.hpp"
#include "Platform.hpp"
#include "Solution.hpp"
#include "SolutionList.hpp"

#ifndef LIBBDD_FUNCTION_HPP_
# define LIBBDD_FUNCTION_HPP_

namespace BDD
{
    namespace Internal
    {
        class Function_p;
    }

    /**
     * @class Function
     * @brief Stores a boolean function.
     * @note This class is refcounted and encapsulate a reference on an object.
     * By using this class you always target the same object even when you copy
     * an instance of this class.
     */
    class BDD_API_PUBLIC Function
    {
        public:
            /**
             * @brief create an empty function.
             * The created function is not a valid function.
             */
            Function();
            /**
             * @brief create a function that represents an
             * anonymous booloean Variable.
             * @param Context the context that will store
             * the BDD associated to this function.
             */
            Function(Context& Context);
            /**
             * @brief create a function that represents a
             * single booloean Variable.
             * @param Context the context that will store
             * the BDD associated to this function.
             * @param Variable the name of the variable.
             */
            Function(Context& Context, const char* Variable);
            Function(Internal::Function_p* Pointer);
            Function(const Function& Fun);
            ~Function();

            /**
             * @brief Test if the function is a valid function
             */
            bool IsValid() const;

            Internal::Context_p* Raw();

            /**
             * @brief Find one solution for this function.
             * @return One solution if there is at leas one solution.
             * @warning the user should test is there is a solution before
             * calling this method.
             */
            Solution FindOneSolution() const;
            /**
             * @brief Find all the solutions for this function.
             * @return A SolutionList containing all the solution.
             * @note The list is empty if the function has no solution.
             */
            SolutionList FindAllSolutions() const;
            /**
             * @brief Test if the function is a Tautology.
             */
            bool IsTautology() const;
            /**
             * @brief Test if the function is a Satisfiable.
             */
            bool IsSatisfiable() const;
            /**
             * @brief Get the number of solution associated to  this function.
             */
            u32  SatCount() const;

            /**
             * @brief Apply a logical AND on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function And(const Function& Func) const;
            /**
             * @brief Apply a logical OR on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function Or(const Function& Func) const;
            /**
             * @brief Apply a logical XOR on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function Xor(const Function& Func) const;
            /**
             * @brief Apply a logical NOT on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function Not();
            /**
             * @brief Apply a logical EXIST on the Function.
             */
            Function LogicalExist(const char* Variable) const;
            /**
             * @brief Apply a logical IMPLIES on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function Implies(const Function& Func) const;
            /**
             * @brief Apply a logical EQUIVALENT on the Function.
             * @note If one of the two functions is not
             * valid then the valid one is returned.
             */
            Function Equivalent(const Function& Func) const;
            /**
             * @brief Replace one variable by another BDD.
             */
            Function Replace(const char* Variable, const Function& Func) const;

            /**
             * @brief Apply a logical AND on the Function.
             * @note If one of the two functions is not
             * valid then properties of the valid one are set inside
             * the current one.
             */
            void InplaceAnd(const Function& Func);
            /**
             * @brief Apply a logical OR on the Function.
             * @note If one of the two functions is not
             * valid then properties of the valid one are set inside
             * the current one.
             */
            void InplaceOr(const Function& Func);
            /**
             * @brief Apply a logical XOR on the Function.
             * @note If one of the two functions is not
             * valid then properties of the valid one are set inside
             * the current one.
             */
            void InplaceXor(const Function& Func);
            /**
             * @brief Apply a logical NOT on the Function.
             */
            void InplaceNot();

            /**
             * @brief Syntactic sugar for the AND operator.
             */
            Function operator &(const Function& Func) const;
            /**
             * @brief Syntactic sugar for the OR operator.
             */
            Function operator |(const Function& Func) const;
            /**
             * @brief Syntactic sugar for the XOR operator.
             */
            Function operator ^(const Function& Func) const;
            /**
             * @brief Syntactic sugar for the NOT operator.
             */
            Function operator !() const;

            /**
             * @brief Syntactic sugar for the inplace AND operator.
             */
            Function& operator &=(const Function& Func);
            /**
             * @brief Syntactic sugar for the inplace OR operator.
             */
            Function& operator |=(const Function& Func);
            /**
             * @brief Syntactic sugar for the inplace XOR operator.
             */
            Function& operator ^=(const Function& Func);
            /**
             * @brief Check if the two functions are equal
             */
            bool operator ==(const Function& Func);
            /**
             * @brief Check if the two functions are different
             */
            bool operator !=(const Function& Func);
            Function& operator =(const Function& Func);

            /**
             * @brief Create a copy of the current instance.
             */
            Function Clone() const;

            void* operator new(size_t size);
            void operator delete(void* ptr);
            void* operator new[](size_t size);
            void operator delete[](void* ptr);

            /**
             * @brief Dump the BDD to a dot like file.
             */
            std::string ToDot() const;

        private:
            Internal::Function_p* _Function;
            u32* _Refcount;
    };

    Function True(Context Ctx);
    Function False(Context Ctx);
}

#endif
