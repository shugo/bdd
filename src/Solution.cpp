/*
 * File : Platform.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <string>

#include "Solution.hpp"
#include "internal/Solution_p.hpp"

namespace BDD
{

    Solution::Solution()
        : _Solution(0),
          _Refcount(0)
    {
    }

    Solution::Solution(const Solution& Sol)
        : _Solution(Sol._Solution),
          _Refcount(Sol._Refcount)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
    }

    Solution::~Solution()
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Solution)
                {
                    delete _Solution;
                    _Solution = 0;
                }
                delete _Refcount;
                _Refcount = 0;
            }
        }
    }

    Solution& Solution::operator =(const Solution& Sol)
    {
        if (_Refcount)
        {
            *_Refcount = *_Refcount - 1;
            if (*_Refcount == 0)
            {
                if (_Solution)
                {
                    delete _Solution;
                }
                delete _Refcount;
            }
        }
        _Solution = Sol._Solution;
        _Refcount = Sol._Refcount;
        if (_Refcount)
        {
            *_Refcount = *_Refcount + 1;
        }
        else
        {
            _Solution = 0;
        }
        return *this;
    }

    void* Solution::operator new(size_t size)
    {
        void* arr = malloc(size * sizeof(Solution));
        memset(arr, 0, size * sizeof(Solution));
        return arr;
    }

    void Solution::operator delete(void* ptr)
    {
        if (ptr)
        {
            free(ptr);
        }
    }

    void* Solution::operator new[](size_t size)
    {
        void* arr = malloc(size * sizeof(Solution));
        memset(arr, 0, size * sizeof(Solution));
        return arr;
    }

    void Solution::operator delete[](void* p)
    {
        if (p)
        {
            free(p);
        }
    }


    Solution::Solution(Internal::Solution_p* Pointer)
        :
            _Solution(Pointer),
            _Refcount(0)
    {
        _Refcount = new int();
        *_Refcount = 1;
    }

    bool Solution::IsValid() const
    {
        return _Solution != 0 && _Refcount != 0;
    }

    bool Solution::GetState(const char* name) const
    {
        return _Solution->GetState(name);
    }

    const std::string Solution::ToString() const
    {
        if (_Solution && _Refcount)
            return _Solution->ToString();
        else
            return "";
    }

}
