/*
 * File : MemoryPool.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <map>
#include <utility>

#include "../Platform.hpp"

#ifndef LIBBDD_MEMORYPOOL_HPP_
# define LIBBDD_MEMORYPOOL_HPP_

namespace BDD
{
    namespace Internal
    {
        struct Node;
        /**
         * @class MemoryPool
         * @brief Memory Pool used to optimize the storage
         * of the relations between nodes.
         * @warning this should be used only by the libbdd.
         */
        class MemoryPool
        {
            public:
                MemoryPool(u32 MaxSize = 0);
                ~MemoryPool();

                void DeleteNode(Node* Node);
                size_t AddNodeId(u32 Variable, size_t OnTrueId, size_t OnFalseId);
                size_t AddNodeId(ptr Variable, size_t OnTrueId, size_t OnFalseId);
                Node* AddNode(u32 Variable, const Node* OnTrue, const Node* OnFalse);
                Node* AddNode(ptr Variable, const Node* OnTrue, const Node* OnFalse);
                Node* GetNodeFromId(size_t Id) const;
                size_t GetIdFromNode(const Node* node) const;
                void DebugDump();
            private:
                s64 _Realloc();
            private:
                u32  _Size;
                u32  _MaxSize;
                Node* _Data;
                Node* _NextData;
                Node* _EndData;
        };
    }
}

#endif // LIBBDD_MEMORYPOOL_HPP_
