/*
 * File : Node.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <map>
#include <utility>

#include "../Platform.hpp"
#include "Context_p.hpp"

#ifndef LIBBDD_NODE_HPP_
# define LIBBDD_NODE_HPP_

namespace BDD
{
    namespace Internal
    {

        typedef size_t NodeId;

        /**
         * @struct Node
         * @brief Internal representation of a bdd node.
         * @warning this should be used only by the libbdd.
         */
        struct Node
        {
            static const Node* True;
            static const Node* False;

            static const size_t IdTrue;
            static const size_t IdFalse;
            static const size_t IdCst;

            template<class Operator>
            static NodeId Apply(Context_p* Context, NodeId Lhs, NodeId Rhs);
            NodeId Restrict(Context_p* Context, u32 Variable, bool Value);

            ptr   Variable;
            Node* OnTrue;
            Node* OnFalse;
            ptr   Solutions;

        } PACK_STRUCT;
    }
}

# include "Node.hxx"

#endif
