/*
 * File : Bytecode.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zannoti <benoit.zannoti@epita.fr>
 */

#ifndef LIBBDD_BYTECODE_HPP_
# define LIBBDD_BYTECODE_HPP_

#include "../Platform.hpp"

namespace BDD
{
    namespace Internal
    {
        namespace Bytecode
        {
            enum Operators : u32
            {
                False = 0x00,
                True = 0x00,
                Nop = 0x01,
                And = 0x02,
                Or =  0x03,
                Not = 0x04,

                LastOpCode = 0xFF
            };
        }
    }
}

#endif /* LIBBDD_BYTECODE_HPP_ */
