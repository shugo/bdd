/*
 * File : Expression.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <cstring>

#include "../Platform.hpp"
#include "Expression.hpp"

namespace BDD
{
    namespace Internal
    {
        Expression::Expression()
            : _Fixed(false),
              _Value(false),
              _Stack(0),
              _StackStart(0),
              _StackSize(0),
              _MaxStackSize(8)
        {
            _Stack = reinterpret_cast<u32*>(::malloc(sizeof(u32) * _MaxStackSize));
            _StackStart = _Stack;
        }

        Expression::~Expression()
        {
            if (_StackStart)
            {
                delete [] _StackStart;
            }
        }

        Expression Expression::Eval(u32 Variable, bool Value)
        {
            u32 instrValue = Value ? Bytecode::True : Bytecode::False;
            u32* newStack = reinterpret_cast<u32*>(malloc(_MaxStackSize));
            u32* newStackStart = newStack;
            u32 newCount = 0;

            //::memccpy(_NewStack, _Stack, _StackSize, sizeof(u32));

            u32* stack = _Stack;
            u32 count = _StackSize;

            while (count)
            {
                u32 instr = *stack;
                if (instr < Bytecode::LastOpCode)
                {
                    switch (instr)
                    {
                        case Bytecode::Nop: { break; }
                        case Bytecode::Not:
                                            {
                                                u32 tos = *newStack;
                                                if (tos == Bytecode::True) { *newStack = Bytecode::False; }
                                                else if (tos == Bytecode::False) { *newStack = Bytecode::True; }
                                                else { ++newStack; *newStack = Bytecode::Not; ++newCount; }
                                                break;
                                            }
                        case Bytecode::And:
                                            {
                                                u32 A = newStack[0];
                                                u32 B = newStack[-1];

                                                if (A == Bytecode::True) {  /* Nothing to do */ }
                                                else if (B == Bytecode::True) { /* Nothing to do */ }
                                                else if (A == Bytecode::False) /* Wrong not all the stack must be erased */ 
                                                { newStack = newStackStart; *newStack = Bytecode::False; newStack++; newCount = 1; }
                                                else if (B == Bytecode::False) /* Wrong not all the stack must be erased */ 
                                                { newStack = newStackStart; *newStack = Bytecode::False; newStack++; newCount = 1; }
                                                else { ++newStack; *newStack = Bytecode::And; ++newCount; }
                                                break;
                                            }
                        case Bytecode::Or:
                                            {
                                                u32 A = newStack[0];
                                                u32 B = newStack[-1];

                                                if (A == Bytecode::False) {  /* Nothing to do */ }
                                                else if (B == Bytecode::False) { /* Nothing to do */ }
                                                else if (A == Bytecode::True) /* Wrong not all the stack must be erased */ 
                                                { newStack = newStackStart; *newStack = Bytecode::True; newStack++; newCount = 1; }
                                                else if (B == Bytecode::True) /* Wrong not all the stack must be erased */ 
                                                { newStack = newStackStart; *newStack = Bytecode::True; newStack++; newCount = 1; }
                                                else { ++newStack; *newStack = Bytecode::And; ++newCount; }
                                                break;
                                            }
                        default:
                                            {
                                                if (Variable == instr) { ++newStack; *newStack = instrValue; ++newCount; break; }
                                                else { ++newStack; *newStack = instr; ++newCount; break;}
                                            }
                    }
                }
                --stack;
                count--;
            }

            Expression e;

            // FIXME
            return e;
        }

        void Expression::Push(u32 value)
        {
            if (_StackSize >= _MaxStackSize)
            {
                _MaxStackSize *= 2;
                _StackStart =
                    reinterpret_cast<u32*>(::realloc(_StackStart, sizeof(u32) * _MaxStackSize));
                _Stack = _StackStart + _StackSize;
            }

            *_Stack = value;
            ++_StackSize;
        }

        bool Expression::TryGetValue(bool* value) const
        {
            if (_Fixed)
            {
                *value = _Value;
                return true;
            }
            return false;
        }
    }
}
