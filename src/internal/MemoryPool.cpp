/*
 * File : MemoryPool.cpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#include <iostream>

#include "../Platform.hpp"
#include "MemoryPool.hpp"
#include "Node.hpp"

namespace BDD
{
    namespace Internal
    {

        MemoryPool::MemoryPool(u32 MaxSize)
            : _Size(32),
              _MaxSize(MaxSize / sizeof(Node)),
              _Data(0),
              _NextData(0)
        {
            _Data = reinterpret_cast<Node*>(::malloc(_Size * sizeof(Node)));
            _NextData = _Data;
            // Ignore the first for optimization reasons
            _NextData++;
            _EndData =_Data + _Size;
            _NextData->Variable = 0;
        }

        MemoryPool::~MemoryPool()
        {
            if (_Data)
            {
                ::free(_Data);
                _Data = 0;
                _NextData = 0;
            }
        }

        void MemoryPool::DeleteNode(Node*)
        {
            // FIXME
        }

        size_t MemoryPool::AddNodeId(u32 Variable, size_t OnTrueId, size_t OnFalseId)
        {
            Node* node = AddNode(Variable, GetNodeFromId(OnTrueId), GetNodeFromId(OnFalseId));
            return GetIdFromNode(reinterpret_cast<Node*>(node));
        }

        size_t MemoryPool::AddNodeId(ptr Variable, size_t OnTrueId, size_t OnFalseId)
        {
            Node* node = AddNode(Variable, GetNodeFromId(OnTrueId), GetNodeFromId(OnFalseId));
            return GetIdFromNode(reinterpret_cast<Node*>(node));
        }


        Node* MemoryPool::AddNode(u32 Variable, const Node* OnTrue, const Node* OnFalse)
        {
            ptr packedVariable =
                reinterpret_cast<ptr>(reinterpret_cast<u8*>(0) + Variable);
            return AddNode(packedVariable, OnTrue, OnFalse);
        }

        Node* MemoryPool::AddNode(ptr Variable, const Node* OnTrue, const Node* OnFalse)
        {
            Node* NoConstOnTrue = const_cast<Node*>(OnTrue);
            Node* NoConstOnFalse = const_cast<Node*>(OnFalse);

            if (_NextData >= _EndData)
            {
                s64 shift = _Realloc();
                if (NoConstOnFalse > Node::True)
                    NoConstOnFalse = reinterpret_cast<Node*>(reinterpret_cast<u8*>(NoConstOnFalse) + shift);
                if (NoConstOnTrue > Node::True)
                    NoConstOnTrue = reinterpret_cast<Node*>(reinterpret_cast<u8*>(NoConstOnTrue) + shift);
            }

            // if next data is outside the previouly
            // allocated data then data must be reallocated.
            Node* newNode = _NextData;
            _NextData++;
            newNode->Variable = Variable;
            newNode->OnTrue = NoConstOnTrue;
            newNode->OnFalse = NoConstOnFalse;

            if (NoConstOnTrue == Node::True)
                newNode->Solutions = reinterpret_cast<u8*>(1);
            else if (OnTrue != Node::False)
                newNode->Solutions = NoConstOnTrue->Solutions;
            else
                newNode->Solutions = 0;

            if (NoConstOnFalse == Node::True)
                newNode->Solutions = reinterpret_cast<u8*>(newNode->Solutions) + 1;
            else if (OnFalse != Node::False)
                newNode->Solutions =
                    reinterpret_cast<u8*>(NoConstOnFalse->Solutions) +
                    reinterpret_cast<size_t>(newNode->Solutions) ;


            if (newNode->OnFalse == newNode->OnTrue)
            {
                if (newNode->OnFalse > reinterpret_cast<const Node*>(Node::True))
                {
                    newNode->Variable = newNode->OnFalse->Variable;
                    newNode->OnFalse = newNode->OnFalse->OnFalse;
                    newNode->OnTrue = newNode->OnTrue->OnTrue;
                }
            }

            if (newNode->OnFalse > reinterpret_cast<const Node*>(Node::True))
                if (newNode->OnFalse->OnFalse == newNode->OnFalse->OnTrue)
                    newNode->OnFalse = newNode->OnFalse->OnFalse;

            if (newNode->OnTrue > reinterpret_cast<const Node*>(Node::True))
                if (newNode->OnTrue->OnFalse == newNode->OnTrue->OnTrue)
                    newNode->OnTrue = newNode->OnTrue->OnTrue;

            return newNode;
        }

        Node* MemoryPool::GetNodeFromId(size_t Id) const
        {
            if (Id < Node::IdCst)
                return reinterpret_cast<Node*>(Id - 1);
            return reinterpret_cast<Node*>(reinterpret_cast<u8*>(_Data) + Id);
        }

        size_t MemoryPool::GetIdFromNode(const Node* node) const
        {
            if (node < reinterpret_cast<Node*>(2))
                return reinterpret_cast<size_t>(reinterpret_cast<const u8*>(node) + 1);
            return reinterpret_cast<const u8*>(node) - reinterpret_cast<u8*>(_Data);
        }

        s64 MemoryPool::_Realloc()
        {
            Node* _OldData = _Data;
            u64 decal = reinterpret_cast<u8*>(_NextData) - reinterpret_cast<u8*>(_Data);
            _Size = _Size * 2;
            _Data = reinterpret_cast<Node*>(::realloc(_Data, sizeof(Node) * _Size));

            _EndData =_Data + _Size;

            // Remap
            if (_OldData != _Data)
            {
                _NextData = reinterpret_cast<Node*>(reinterpret_cast<u8*>(_Data) + decal);

                s64 shift = (reinterpret_cast<u8*>(_Data) - reinterpret_cast<u8*>(_OldData));

                register Node* mem = _Data;

                while (mem < _NextData)
                {
                    if (mem->OnTrue > reinterpret_cast<Node*>(1))
                        mem->OnTrue =
                            reinterpret_cast<Node*>(reinterpret_cast<u8*>(mem->OnTrue) + shift);
                    if (mem->OnFalse > reinterpret_cast<Node*>(1))
                        mem->OnFalse =
                            reinterpret_cast<Node*>(reinterpret_cast<u8*>(mem->OnFalse) + shift);
                    ++mem;
                }

                return shift;
            }
            return 0;
        }

        void MemoryPool::DebugDump()
        {
            register ptr* bst = reinterpret_cast<ptr*>(_Data);
            while (bst < reinterpret_cast<ptr*>(_NextData))
            {
                std::cout << "node (" << bst << ") = [Left:";
                bst++;
                std::cout << bst << ";Right:";
                bst++;
                std::cout << bst << "]" << std::endl;
                bst++;
                bst++;
            }
        }
    }
}
