/*
 * File : LogicalFunctions.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_LOGICALFUNCTIONS_HPP_
# define LIBBDD_LOGICALFUNCTIONS_HPP_

namespace BDD
{
    namespace Internal
    {
        namespace LogicalFunctions
        {
            struct And
            {
                static bool Act(bool a, bool b) { return a & b; }
                static const char Value = 0;
            };

            struct Or
            {
                static bool Act(bool a, bool b) { return a | b; }
                static const char Value = 1;
            };

            struct Xor
            {
                static bool Act(bool a, bool b) { return a ^ b; }
                static const char Value = 2;
            };

            struct Implies
            {
                static bool Act(bool a, bool b) { return !a || b; }
                static const char Value = 6;
            };
        }
    }
}

#endif /* !LIBBDD_LOGICALFUNCTIONS_HPP_ */
