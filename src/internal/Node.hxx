/*
 * File : Node.hxx
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_NODE_HXX_
# define LIBBDD_NODE_HXX_

namespace BDD
{
    namespace Internal
    {
        template<class Operator>
        NodeId Node::Apply(Context_p* Context, NodeId Lhs, NodeId Rhs)
        {
            bool LhsIsTF = (Lhs < IdCst);
            bool RhsIsTF = (Rhs < IdCst);

            if (LhsIsTF && RhsIsTF)
            {
                return (Operator::Act(Lhs == IdTrue, Rhs == IdTrue) ? IdTrue : IdFalse);
            }

            NodeId Result = Context->GetCachedResult(Lhs, Rhs, Operator::Value);

            if (Result != 0)
            {
                return Result;
            }


            MemoryPool* memPool = Context->GetMemoryPool();

            Node* LhsPtr = memPool->GetNodeFromId(Lhs);
            Node* RhsPtr = memPool->GetNodeFromId(Rhs);

            if (RhsIsTF || (!LhsIsTF && LhsPtr->Variable < RhsPtr->Variable))
            {
                ptr Variable = LhsPtr->Variable;
                NodeId OnTrue = Apply<Operator>(Context, memPool->GetIdFromNode(LhsPtr->OnTrue), Rhs);

                /* Pointers may have change because of the insertion */
                LhsPtr = memPool->GetNodeFromId(Lhs);
                NodeId OnFalse = Apply<Operator>(Context, memPool->GetIdFromNode(LhsPtr->OnFalse), Rhs);

                /* Create the Result */
                Result = Context->AddNode(POINTER_TO_U32(Variable), OnTrue, OnFalse);
            }
            else if (LhsIsTF || (LhsPtr->Variable > RhsPtr->Variable))
            {
                ptr Variable = RhsPtr->Variable;

                NodeId OnTrue = Apply<Operator>(Context, Lhs, memPool->GetIdFromNode(RhsPtr->OnTrue));
                /* Pointers may have change because of the insertion */
                RhsPtr = memPool->GetNodeFromId(Rhs);
                NodeId OnFalse = Apply<Operator>(Context, Lhs, memPool->GetIdFromNode(RhsPtr->OnFalse));

                /* Create the Result */
                Result = Context->AddNode(POINTER_TO_U32(Variable), OnTrue, OnFalse);
            }
            else // (Lhs->Variable == Rhs->Variable)
            {
                ptr Variable = LhsPtr->Variable;

                NodeId OnTrue = Apply<Operator>(Context,
                        memPool->GetIdFromNode(LhsPtr->OnTrue),
                        memPool->GetIdFromNode(RhsPtr->OnTrue));

                /* Pointers may have change because of the insertion */
                RhsPtr = memPool->GetNodeFromId(Rhs);
                LhsPtr = memPool->GetNodeFromId(Lhs);

                NodeId OnFalse = Apply<Operator>(Context,
                        memPool->GetIdFromNode(LhsPtr->OnFalse),
                        memPool->GetIdFromNode(RhsPtr->OnFalse));

                /* Create the Result */
                Result = Context->AddNode(POINTER_TO_U32(Variable), OnTrue, OnFalse);
            }

            Context->SetCachedResult(Lhs, Rhs, Operator::Value, Result);
            return Result;
        }
    }
}


#endif /* !LIBBDD_NODE_HXX_ */
