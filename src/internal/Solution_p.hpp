/*
 * File : Solution_p.hpp
 *
 * Authors :
 *    Raphael 'Shugo' Boissel <raphael.boissel@epita.fr>
 *    Augustin 'Yoopo' Cheron <augustin.cheron@epita.fr>
 *    Benoit 'Jicks' Zanotti <benoit.zanotti@epita.fr>
 */

#ifndef LIBBDD_SOLUTION_P_HPP_
# define LIBBDD_SOLUTION_P_HPP_

# include <map>
# include <string>

namespace BDD
{
    namespace Internal
    {
        /**
         * @struct Node
         * @brief Internal representation of a solution.
         * @warning this should be used only by the libbdd.
         */
        class Solution_p
        {
            public:
                Solution_p();
                bool GetState(const std::string name) const;
                void SetState(const std::string name, bool value);
                Solution_p* Clone() const;
                std::string ToString() const;
            private:
                std::map<std::string, bool> _Variables;
        };
    }
}

#endif // LIBBDD_SOLUTION_P_HPP_
