#! /usr/bin/lua

function exist(file)
	return io.open(file) ~= nil
end

function run(compiler, testname, code)

	code = "int main(){\n"..code.."\nreturn 0;\n}\n"
	code = "#include <Function.hpp>\n"..code
	code = "#include <Context.hpp>\n"..code
	code = "#include <iostream>\n"..code
	code = "#include <vector>\n"..code
	code = "#include <string>\n"..code
	code = "#include <sstream>\n"..code
	code = "#define PASS (0)\n"..code
	code = "#define FAIL (1)\n"..code
	os.remove("temp.cpp");
	file = io.open("temp.cpp", "w+");
	file:write(code);
	file:close();
	local exitCode = os.execute('"'..compiler..'"'.." ".."-I ../src ".." temp.cpp ".." ./libbdd.so -o temp.exe")
	os.remove("temp.cpp");

	if (exist("temp.exe") == false) then
		print("\27[0;31m[FAIL]\27[m Compilation fail : ", testname)
		return false
	end

	terminated, exit_type, exit_code = os.execute("./temp.exe");
	sec = os.time()
	os.remove("temp.exe")
	sec = os.time() - sec

	-- lua_execresult doesn't push 3 value on some version
	-- check if the stack has 3 value
	if (exit_code ~= nil) then
		if (terminated and exit_code == 0) then
			print("\27[0;32m[PASS]\27[m ", testname, sec.." s")
			return true
		else if (exit_type == "exit") then
	       print("\27[0;31m[FAIL]\27[m ", testname, sec.." s")
		else
	       print("\27[0;31m[XXXX]\27[m ", testname, sec.." s")
		end end
	else
		exit_code = terminated
		if (exit_code == 0) then
			print("\27[0;32m[PASS]\27[m ", testname, sec.." s")
			return true
		else
	       print("\27[0;31m[FAIL]\27[m ", testname, sec.." s")
		end
	end
	return false
end

local compiler = os.getenv("CXX")
if (complile == nil) then compiler = "/bin/g++" end
if (exist(compiler) == false) then compiler = "/usr/bin/g++" end
if (exist(compiler) == false) then compiler = "/usr/bin/clang++" end
if (exist(compiler) == false) then compiler = 'C:\\cygwin\\bin\\g++-4.exe' end
if (exist(compiler) == false) then compiler = 'C:\\cygwin\\bin\\g++.exe'end
if (exist(compiler) == false) then
	compiler = 'C:\\Program Files (x86)\\Microsoft Visual Studio 9.0\\VC\\bin\\cl.exe'
end
if (exist(compiler) == false) then exit(1) end

fail = false;

testList = io.open("Testlist", "r+");
local testcount = 0
local passcount = 0
local testname = ""
for line in testList:lines() do
	local disp = string.gmatch(line, "^--%-?%s*(.*)$")
	local ignore = false
	for text in disp do
		if (testcount ~= 0) then
			print (passcount.."/"..testcount)
			print ("")
		end
		print(text)
		testname = text
		ignore = true
		testcount = 0
		passcount = 0
	end
	if ignore == false then
		line = string.gsub(line, "(\r)", "")
		if (line ~= "") then
			local test = io.open(line, "r+")
			if (test ~= nil) then
				local code = ""
				for codeline in test:lines() do
					code = code..codeline.."\n"
				end
				if (run(compiler, line, code) == true) then
					passcount = passcount + 1
				end
				test:close()
			else
				local errmsg = "\27[0;31m[FAIL]\27[m file not found : "
				print(errmsg..line)
			end
			testcount = testcount + 1
		end

	end
end

if (testcount ~= 0) then
			print (passcount.."/"..testcount)
			print ("")
			if (passcount ~= testcount) then fail = true end
end

if (fail) then os.exit(42) end
testList:close()




