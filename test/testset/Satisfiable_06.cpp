BDD::Context ctx;

BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c(ctx, "c");
BDD::Function d(ctx, "d");

a = ((a.Implies(b)) & (b & !a)) | (c.Implies(d) & (c & d & a & b));
if (!a.IsSatisfiable())
	return FAIL;