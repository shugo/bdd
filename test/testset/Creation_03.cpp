BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c(ctx, "c");
for (int n = 0; n < 10000; n++)
{
	a =  a | b | (!b);
	a =  a | b | (!a) & c;
	c = a | b & c;
	b = !a & (!c | b);
}