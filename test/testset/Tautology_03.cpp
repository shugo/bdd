BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c(ctx, "c");
BDD::Function d = ((a | b | c) | ((!a) & (!b) & (!c)));
if (!d.IsTautology())
	return FAIL;