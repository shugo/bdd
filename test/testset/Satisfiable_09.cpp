BDD::Context ctx;

BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
BDD::Function c = (a.Implies(!a & !b)).Equivalent(!a);

if (!c.IsTautology())
	return FAIL;
