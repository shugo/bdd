BDD::Context ctx;
BDD::Function a(ctx, "a");
for (int n = 0; n < 10000; n++)
{
	a =  a | (!a);
}

if (!a.IsTautology())
	return FAIL;