BDD::Context context;

BDD::Function s =
    BDD::Function(context, "a") |
    BDD::Function(context, "b") |
    BDD::Function(context, "c");

BDD::Function r = BDD::Function(context, "d") | BDD::Function(context, "a");
r = s.Replace("a", r);
r = r | BDD::Function(context, "!a");
    
if (!r.IsTautology())
	return FAIL;