  int nRooks = 8;
  BDD::Context ctx;
  BDD::Function result;
  BDD::Function** x;

  /* Initialization of the chess board variable */
  x = new BDD::Function*[nRooks];
  for (int i = 0; i < nRooks; i++)
  {
    x[i] = new BDD::Function[nRooks];
    for (int j = 0; j < nRooks; j++)
    {
      std::stringstream ss;
      ss << "x(" << i << "," << j << ")";
      x[i][j] = BDD::Function(ctx, ss.str().c_str());
    }
  }

  /* Ensuring at most one rook by row, column and diagonals */
  for (int i = 0; i < nRooks; i++)
    for (int j = 0; j < nRooks; j++)
    {
      BDD::Function atMostOne;

      for (int l = 0; l < nRooks; l++)
        if (l != j)
          atMostOne &= !x[i][l];

      for (int k = 0; k < nRooks; k++)
        if (k != i)
          atMostOne &= !x[k][j];

      result &= x[i][j].Implies(atMostOne);
    }

  /* Ensuring at least one rook by row */
  for (int i = 0; i < nRooks; i++)
  {
    BDD::Function row;
    for (int j = 0; j < nRooks; j++)
      row |= x[i][j];
    result &= row;
  }

  if(result.SatCount() != 40320)
	  return FAIL;