BDD::Context ctx;

BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");

a = (a ^ b) & (a & b);
if (a.IsSatisfiable())
	return FAIL;