BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");

if ((!a & !b).SatCount() != 1)
  return FAIL;
