BDD::Context ctx;
BDD::Function a(ctx, "a");
BDD::Function b(ctx, "b");
a = ((a & (!b))| (b & (!a))) & (!a);

if (!a.IsSatisfiable())
	return FAIL;