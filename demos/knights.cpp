#include <iostream>
#include <sstream>
#include <vector>
#include <utility>
#include <string>
#include <cstring>
#include <Context.hpp>
#include <Function.hpp>

enum Command {
    Count,
    DisplayOne,
    DisplayAll
};

class Board
{
    public:
        Board(BDD::Context ctx, unsigned int size)
            : _Ctx(ctx),
              _Size(size)
    {
        x = new BDD::Function*[_Size];
        for (unsigned int i = 0; i < _Size; i++)
        {
            x[i] = new BDD::Function[_Size];
            for (unsigned int j = 0; j < _Size; j++)
            {
                std::stringstream ss;
                ss << "x(" << i << "," << j << ")";
                x[i][j] = BDD::Function(_Ctx, ss.str().c_str());
            }
        }
    }

        ~Board()
        {
            for (unsigned int i = 0; i < _Size; i++)
                delete[] x[i];
            delete[] x;
        }

        BDD::Function operator()(int i, int j)
        {
            return x[i][j];
        }

    private:
        BDD::Context _Ctx;
        unsigned int _Size;
        BDD::Function** x;

};

std::vector<std::pair<int, int> > reachable(int i, int j, int Size)
{
    std::vector<std::pair<int, int> > result;
    int d[8][2] = {{-1,-2},{-1,2},{1,-2},{1,2},{-2,-1},{-2,1},{2,-1},{2,1}};

    for (int k = 0; k < 8; k++)
        if ((i + d[k][0] >= 0 && i + d[k][0] < Size) &&
                (j + d[k][1] >= 0 && j + d[k][1] < Size))
            result.push_back(std::make_pair(i + d[k][0], j + d[k][1]));

    return result;
}

static BDD::Function MakeTransitions(BDD::Function r, Board& board, unsigned int boardSize)
{
    for (unsigned int i = 0; i < boardSize; i++)
    {
        for (unsigned int j = 0; j < boardSize; j++)
        {
            std::vector<std::pair<int, int> > reach = reachable(i, j, boardSize);
            BDD::Function transitions;
            for (unsigned int k = 0; k < reach.size(); k++)
            {
                transitions &= board(reach[k].first, reach[k].second);
            }
            std::stringstream ss;
            ss << "x(" << i << "," << j << ")";
            r &= (board(i, j).Equivalent(transitions));
        }
    }
    return r;
}

static void knightsProblem(int boardSize)
{
    BDD::Context ctx;
    Board board(ctx, boardSize);
    //Start at the lower left corner
    BDD::Function initialState = board(boardSize - 1, 0);
    BDD::Function result = initialState;
    BDD::Function step;
    BDD::Function transition;
    BDD::Function fullBoard;

    do {
        step = result.Clone();
        result = MakeTransitions(result, board, boardSize);
    } while (result != step);


    // Check if some cell has not been visited
    for (int i = 0; i < boardSize; i++)
        for (int j = 0; j < boardSize; j++)
            fullBoard |= !board(i, j);

    result = fullBoard & (result);

    /* Displaying the result */
    unsigned int count = result.SatCount();
    if (count > 0)
    {
        std::cout << "Some cell are unreachable." << std::endl;
    }
    else
    {
        std::cout << "The full board can be visited." << std::endl;
    }

}

static std::string usage(std::string name)
{
    return name + " is a solver of the knights problem.\n"
        + "Its options are:\n"
        + "\t--help\t\tDisplay the help.\n"
        + "\t-n N\t\tSet the positive size of the problem (default:8).\n";
}

int main(int argc, char** argv)
{
    int boardSize = 8;
    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-n") && (i + 1) < argc)
            boardSize = atoi(argv[++i]);
        else if (!strcmp(argv[i], "--help"))
        {
            std::cout << usage(argv[0]) << std::endl;
            return 0;
        }
        else
        {
            std::cerr << usage(argv[0]) << std::endl;
            return 1;
        }
    }

    if (boardSize > 0)
        knightsProblem(boardSize);
    else
        std::cerr << "Size should be positive." << std::endl;

    return boardSize <= 0;
}
