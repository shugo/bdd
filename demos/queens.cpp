#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>
#include <Context.hpp>
#include <Function.hpp>

enum Command {
    Count,
    DisplayOne,
    DisplayAll
};

static BDD::Function make(BDD::Context ctx, BDD::Function** x, std::vector<std::vector<bool>>& p, int row, int nQueens)
{
    if (row >= nQueens)
        return BDD::True(ctx);

    BDD::Function result;
    std::vector<std::vector<bool>> possible;
    possible.resize(nQueens);
    for (int i = 0; i < nQueens; i++)
        possible[i] = p[i];

    for (int j = 0; j < nQueens; j++)
    {
        if (!possible[row][j])
        {
            result &= !x[row][j];
            continue;
        }
        for (int i = row + 1; i < nQueens; i++)
            possible[i][j] = false;
        for (int k = 1; k < nQueens; k++)
        {
            if (row + k < nQueens && j + k < nQueens)
                possible[row + k][j + k] = false;
            if (row + k < nQueens && j - k >= 0)
                possible[row + k][j - k] = false;
        }
        result &= x[row][j].Implies(make(ctx, x, possible, row + 1, nQueens));
        for (int i = row + 1; i < nQueens; i++)
            possible[i][j] = p[i][j];
        for (int k = 1; k < nQueens; k++)
        {
            if (row + k < nQueens && j + k < nQueens)
                possible[row + k][j + k] = p[row + k][j + k];
            if (row + k < nQueens && j - k >= 0)
                possible[row + k][j - k] = p[row + k][j - k];
        }
    }
    return result;
}

static void queensProblem(int nQueens, enum Command command)
{
    BDD::Context ctx;
    BDD::Function result;
    BDD::Function** x;
    std::vector<std::vector<bool>> possible(nQueens, std::vector<bool>(nQueens, true));

    /* Initialization of the chess board variable */
    x = new BDD::Function*[nQueens];
    for (int i = 0; i < nQueens; i++)
    {
        x[i] = new BDD::Function[nQueens];
        for (int j = 0; j < nQueens; j++)
        {
            std::stringstream ss;
            ss << "x(" << i << "," << j << ")";
            x[i][j] = BDD::Function(ctx, ss.str().c_str());
        }
    }

    result = make(ctx, x, possible, 0, nQueens);

    for (int i = 0; i < nQueens; i++)
    {
        BDD::Function row;
        for (int j = 0; j < nQueens; j++)
            row |= x[i][j];
        result &= row;
    }

    /* Displaying the result */
    switch (command)
    {
        case Count:
            std::cout << "Number of solutions for the " << nQueens << " queens problem is: "
                << result.SatCount() << "." << std::endl;
            break;
        case DisplayOne:
            if (result.SatCount() > 0)
                std::cout << "One of the solutions of the " << nQueens << " queens problem is: "
                    << std::endl << result.FindOneSolution().ToString() << std::endl;
            else
                std::cout << "There is no solutions to the " << nQueens << " queens problem."
                    << std::endl;
            break;
        case DisplayAll:
            if (result.SatCount() > 0)
                std::cout << "The solutions of the " << nQueens << " queens problem are: "
                    << std::endl << result.FindAllSolutions().ToString() << std::endl;
            else
                std::cout << "There is no solutions to the " << nQueens << " queens problem."
                    << std::endl;
            break;
    }

    for (int i = 0; i < nQueens; i++)
        delete[] x[i];
    delete[] x;
}

static std::string usage(std::string name)
{
    return name + " is a solver of the N queens problem.\n"
        + "Its options are:\n"
        + "\t--help\t\tDisplay the help.\n"
        + "\t-n N\t\tSet the positive size of the problem (default:8).\n"
        + "\t--count\t\tDisplay the number of solutions for the problem (default command).\n"
        + "\t--displayOne\tDisplay one solution (if it exists).\n"
        + "\t--displayAll\tDisplay all the possible solutions.\n";
}

int main(int argc, char** argv)
{
    int nQueens = 8;
    enum Command command = Count;
    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-n") && (i + 1) < argc)
            nQueens = atoi(argv[++i]);
        else if (!strcmp(argv[i], "--displayOne"))
            command = DisplayOne;
        else if (!strcmp(argv[i], "--displayAll"))
            command = DisplayAll;
        else if (!strcmp(argv[i], "--count"))
            command = Count;
        else if (!strcmp(argv[i], "--help"))
        {
            std::cout << usage(argv[0]) << std::endl;
            return 0;
        }
        else
        {
            std::cerr << usage(argv[0]) << std::endl;
            return 1;
        }
    }

    if (nQueens > 0)
        queensProblem(nQueens, command);
    else
        std::cerr << "Size should be positive." << std::endl;

    return nQueens <= 0;
}
