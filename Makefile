SRC = \
src/internal/Context_p.cpp  \
src/internal/Expression.cpp \
src/internal/Function_p.cpp \
src/internal/MemoryPool.cpp \
src/internal/Node.cpp \
src/internal/Solution_p.cpp \
src/Function.cpp \
src/Context.cpp \
src/Solution.cpp \
src/SolutionList.cpp

CXXFLAGS = -W -Wall -Wextra -O3 -fPIC -std=c++0x
DEFINES =

OBJ = $(SRC:.cpp=.o)

all: libbdd.so documentation demos

libbdd.so: $(OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS) -shared

%.o: %.cpp
	$(CXX) -c -o $@ $^ $(CXXFLAGS) $(DEFINES) 


check: libbdd.so
	@$(MAKE) -s -C test

clean:
	rm -rf $(OBJ)
	rm -rf doc/doc
	$(MAKE) -C test clean
	$(MAKE) -C demos clean

documentation:
	$(MAKE) -C doc

mrproper: clean
	rm -rf libbdd.so

demos: libbdd.so
	$(MAKE) -C demos

PWD=$(shell pwd)
depend.mk: $(SRC)
	@echo Making depedencies
	@$(CXX) $(CXXFLAGS) $(SRC) -MM | \
		sed 's|\(.*\):|$(PWD)/\1 $(PWD)/\1r $(PWD)/\1d $(PWD)/\1p:|' > depend.mk

-include depend.mk
